# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "ubuntu/trusty64"

  #config.vm.box_download_insecure = true

  # Using vagrant-cachier plugin to avoid downloading packages each time VM is provisioned.
  if Vagrant.has_plugin?("vagrant-cachier")
    config.cache.scope = :box
  end

  # A Vagrant plugin that configures the virtual machine to use specified proxies
  # See: http://tmatilai.github.io/vagrant-proxyconf/
  if Vagrant.has_plugin?("vagrant-proxyconf")
    config.proxy.http     = ENV["VAGRANT_HTTP_PROXY"]
    config.proxy.https    = ENV["VAGRANT_HTTPS_PROXY"]
    config.proxy.no_proxy = ENV["VAGRANT_NO_PROXY"]
  end

  config.vm.network "forwarded_port", guest: 8081, host: 9000

  config.vm.network "private_network", ip: "192.168.33.10"

  config.vm.provision "shell", path: "provisioning/setup.sh"

  config.vm.provider "virtualbox" do |vb|
    # Display the VirtualBox GUI when booting the machine
    vb.gui = false

    # Customize the amount of memory on the VM
    vb.memory = "2048"
  end
end
