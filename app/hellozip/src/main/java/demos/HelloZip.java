package demos;

import java.io.File;
import java.io.FileNotFoundException;

import java.util.Scanner;

public class HelloZip {

    public static void main(String[] args) {
        System.out.println( "Hello Zip!" );

        new HelloZip().processFiles("databases");
    }

    private void processFiles(String databaseDir) {
        File[] databaseFiles = new File(databaseDir).listFiles();

        if (databaseFiles.length == 0) {
          System.out.println("No database files found in dir: " + databaseDir);
          return;
        }

        System.out.println("Found " + databaseFiles.length + " database files in dir: " + databaseDir);

        for (File databaseFile : databaseFiles) {
            try {
              if (databaseFile.isFile()) {
                  printContentsAsText(databaseFile);
              }
            } catch (FileNotFoundException e) {
              System.err.println(e);
            }
        }
    }

    private void printContentsAsText(File databaseFile) throws FileNotFoundException {
      System.out.println("===== " + databaseFile + " =====");
      Scanner fileScanner = new Scanner(databaseFile);
      while (fileScanner.hasNextLine()) {
        System.out.println(fileScanner.nextLine());
      }
    }
}
