#!/bin/bash

nexus_version=2.11.4-01
nexus_bundle=/vagrant/cache/nexus-${nexus_version}-bundle.zip

sudo_cmd="sudo -E"

${sudo_cmd} apt-get update
${sudo_cmd} apt-get install -y git zip unzip maven software-properties-common python-software-properties

# Install JDK 8 via a PPA
# See: http://www.webupd8.org/2012/09/install-oracle-java-8-in-ubuntu-via-ppa.html
# ${sudo_cmd} bash -c "echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections" && \
# add-apt-repository -y ppa:webupd8team/java && \
# apt-get update && \
# apt-get install -y oracle-java8-installer"

# Install JDK 8 manually without a PPA (use this method when above doesn't work, e.g. corporate firewall issues)
# See: http://www.webupd8.org/2014/03/how-to-install-oracle-java-8-in-debian.html
dpkg -l oracle-java8-installer
if [[ $? != 0 ]]; then
  ${sudo_cmd} bash -c "echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections"
  ${sudo_cmd} bash -c 'echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee /etc/apt/sources.list.d/webupd8team-java.list'
  ${sudo_cmd} bash -c 'echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" | tee -a /etc/apt/sources.list.d/webupd8team-java.list'
  ${sudo_cmd} apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys EEA14886
  ${sudo_cmd} apt-get update
  ${sudo_cmd} apt-get install -y oracle-java8-installer
fi

echo -e "# Nexus environment variables\n" > /tmp/nexus.sh
echo -e "export NEXUS_HOME=/opt/nexus\n" >> /tmp/nexus.sh
echo -e "export http_proxy=${http_proxy}\n" >> /tmp/nexus.sh
echo -e "export https_proxy=${https_proxy}\n" >> /tmp/nexus.sh
echo -e "export no_proxy=localhost,192.168.33.10\n" >> /tmp/nexus.sh

${sudo_cmd} mv /tmp/nexus.sh /etc/profile.d/nexus.sh
${sudo_cmd} chmod a+x /etc/profile.d/nexus.sh

if [ ! -f /opt/nexus/bin/nexus ]; then
  if [[ ! -f ${nexus_bundle} ]]; then
    echo "Missing Nexus bundle file: ${nexus_bundle}"
    exit 1
  fi

  echo "*** Installing Nexus ..."

  ${sudo_cmd} mkdir -p /opt
  cd /opt
  ${sudo_cmd} unzip ${nexus_bundle}
  ${sudo_cmd} ln -s /opt/nexus-${nexus_version} /opt/nexus
fi

JAVA_HOME=/usr/lib/jvm/java-8-oracle

if [ ! -f /etc/profile.d/jdk.sh ]; then
   echo -e "export JAVA_HOME=${JAVA_HOME}\n" > /tmp/jdk.sh
   echo -e "export PATH=\${JAVA_HOME}/bin:\${PATH}" >> /tmp/jdk.sh
   ${sudo_cmd} mv /tmp/jdk.sh /etc/profile.d/jdk.sh
   ${sudo_cmd} chmod a+x /etc/profile.d/jdk.sh
fi

grep -q "set.JAVA_HOME" /opt/nexus/bin/jsw/conf/wrapper.conf
if [[ $? != 0 ]]; then
  ${sudo_cmd} bash -c "echo set.JAVA_HOME=${JAVA_HOME} >> /opt/nexus/bin/jsw/conf/wrapper.conf"
fi

grep -q "wrapper.java.command=%JAVA_HOME%/bin/java" /opt/nexus/bin/jsw/conf/wrapper.conf
if [[ $? != 0 ]]; then
  ${sudo_cmd} bash -c "echo wrapper.java.command=%JAVA_HOME%/bin/java >> /opt/nexus/bin/jsw/conf/wrapper.conf"
fi

# To install and run Nexus as a service see: http://books.sonatype.com/nexus-book/reference/install-sect-service.html
${sudo_cmd} RUN_AS_USER=root /opt/nexus/bin/nexus stop
sleep 5
${sudo_cmd} RUN_AS_USER=root /opt/nexus/bin/nexus start
