Nexus / Maven Artefact Demo
===========================

Demonstration of using Nexus as an artifact repository and managing ZIP artifacts with Maven.

**Note:** This demo is based on Nexus OSS v2.11.

Refer to the Nexus OSS [documentation](http://books.sonatype.com/nexus-book/reference/index.html).

Setup
-----

* Install Virtualbox
* Install Vagrant
* Install [Vagrant Cachier plugin](https://github.com/fgrehm/vagrant-cachier) (optional)
* Download Nexus OSS and put it in the `cache` dir (`cache/nexus-2.11.4-01-bundle.zip`)

Start VM
--------

* Open a terminal/console window

* If you are behind a corporate proxy, install the `vagrant-proxyconf` plugin

```
# Windows
set HTTP_PROXY=<your_http_proxy>
set HTTPS_PROXY=<your_https_proxy>
set NO_PROXY=<your_no_proxy>

# Mac/Linux (in Bash)
export http_proxy=<your_http_proxy>
export https_proxy=<your_https_proxy>
export no_proxy=<your_no_proxy>

vagrant plugin install vagrant-proxyconf
```

*  Optional, install `vagrant-cachier` plugin (to speed up rebuilding the VM by using local cache):

```
vagrant plugin install vagrant-cachier
```

* Set the following environment variables:

```
# Windows
set VAGRANT_HTTP_PROXY=<your_http_proxy>
set VAGRANT_HTTPS_PROXY=<your_https_proxy>
set VAGRANT_NO_PROXY=<your_no_proxy>

# Mac/Linux (in Bash)
export VAGRANT_HTTP_PROXY=<your_http_proxy>
export VAGRANT_HTTPS_PROXY=<your_https_proxy>
export VAGRANT_NO_PROXY=<your_no_proxy>
```

* Execute: `vagrant up --provision`

Running Nexus
-------------
* Nexus should automatically start after the VM is booted and provisioned
* To manually start Nexus: `sudo -E RUN_AS_USER=root /opt/nexus/bin/nexus start`
* To manually stop Nexus: `sudo -E RUN_AS_USER=root /opt/nexus/bin/nexus stop`

Access VM terminal
------------------

* Open console
* Execute: `vagrant ssh` - required `ssh` to be in your path (on Windows, install mingw or cygwin, for example)

Access Nexus web interface
--------------------------

* Browse to [http://localhost:9000/nexus](http://localhost:9000/nexus) from your host
* Admin user credentials are:
    * Username: `admin`
    * Password: `admin123`
* Maven uses the Deployment user credentials:
    * Username: `deployment`
    * Password: `deployment123`

Shutdown VM
-----------

* Open console
* Execute: `vagrant destroy` to remove VM or `vagrant suspend` to keep the VM around for future use.

Using the Maven Deploy Plugin
-----------------------------

To deploy the ZIP artefact files:

    mvn deploy:deploy-file -DgroupId=demos -DartifactId=chucknorrisdb -Dversion=1.0.0 -Dpackaging=zip -Dfile=source-dbs/chucknorrisdb-1.0.0.zip -DrepositoryId=deployment -Durl=http://192.168.33.10:8081/nexus/content/repositories/releases/ --settings ../settings.xml

    mvn deploy:deploy-file -DgroupId=demos -DartifactId=murphylawsdb -Dversion=1.0.0 -Dpackaging=zip -Dfile=source-dbs/murphylawsdb-1.0.0.zip -DrepositoryId=deployment -Durl=http://192.168.33.10:8081/nexus/content/repositories/releases/ --settings ../settings.xml

To publish the demos:hellozip:jar:1.0-SNAPSHOT artifact:

    cd app/hellozip
    mvn deploy --settings ../settings.xml

To deploy an arbitrary JAR file:

    mvn deploy:deploy-file -DgroupId=demos -DartifactId=hellozip -Dversion=1.0-SNAPSHOT -Dpackaging=jar -Dfile=target/hellozip-1.0-SNAPSHOT.jar -DrepositoryId=deployment -Durl=http://192.168.33.10:8081/nexus/content/repositories/snapshots/ -DpomFile=pom.xml --settings ../settings.xml

Execute the Java program
------------------------

    mvn package exec:java --settings ../settings.xml


Cleanup database files
----------------------

    mvn clean --settings ../settings.xml

    # databases/*.txt will be removed

Notes:
------

* This demo uses the Vagrant box: https://atlas.hashicorp.com/ubuntu/boxes/trusty64
* If you are behind a corporate firewall you will need to configure your HTTP_PROXY/HTTPS_PROXY settings
* When you execute `vagrant up` the box will be downloaded automatically
* Demo app created using:

```sh
cd app
mvn archetype:create -DgroupId=demos -DartifactId=hellozip -DarchetypeArtifactId=maven-archetype-quickstart --settings ../config/m2_settings.xml
```
* Executable jar created using the [Shade plugin](https://maven.apache.org/plugins/maven-shade-plugin/examples/executable-jar.html)

References:
-----------
* [Maven Lifecycles and Phases](https://maven.apache.org/ref/3.3.3/maven-core/lifecycles.html)
* [Apache Maven Dependency Plugin](https://maven.apache.org/plugins/maven-dependency-plugin/usage.html)
* More on [deploy:deploy-file](https://maven.apache.org/plugins/maven-deploy-plugin/deploy-file-mojo.html)
* [Configuring Maven to deploy to Nexus](https://support.sonatype.com/entries/21283268-Configure-Maven-to-Deploy-to-Nexus)
* [Maven Settings](https://maven.apache.org/settings.html)
